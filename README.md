# jupiterhub_ansible

## How to use
1. Clone this repo and cd to jupyterhub_ansible folder
2. Install ansible and python
3. Set vars in roles/jupyter_install/defaults/main.yaml if needed
4. Run command:
```bash
ansible-playbook install_jupyterhub.yaml --private-key "<path_to_private_key>" -u <remote_username> -i hosts -e "ansible_port=<remote_port>" -e "ansible_sudo_pass=<remote_user_sudo_password>"
```
example:
```bash
 ansible-playbook install_jupyterhub.yaml --private-key "/home/user/.ssh/private_key" -u user -i hosts -e "ansible_port=22" -e "ansible_sudo_pass=PasS11"
 ```